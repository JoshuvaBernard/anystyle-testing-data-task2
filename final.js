const axios = require("axios");
const fs = require("fs");
const XLSX = require("xlsx");
const JSSoup = require("jssoup").default;

function sheet_to_json(path) {
  /*
    To convert a excel sheet to json
    Args: 
      * path(string) - Path to the excell file
    Returns:
      * data - An array of objects with keys Customer, Project, Article_Id, Manuscript
  */
  const workbook = XLSX.readFile(path);
  let worksheets = {};
  for (const sheetName of workbook.SheetNames) {
    worksheets[sheetName] = XLSX.utils.sheet_to_json(
      workbook.Sheets[sheetName]
    );
  }
  // to access different sheets just add the sheet number. i.e Sheet2 for second sheet
  let data = worksheets.Sheet2;
  console.log("Received data from excel");
  return data;
}

function get_data(url) {
  /*
    To get the the data from the url 
    Args: 
      * url(string) - url of te page
    Returns:
      * res.data - 
  */
  return new Promise(function (resolve, reject) {
    axios
      .get(url)
      .then((res) => {
        console.log(`Received data from the url, statusCode: ${res.status}`);

        return resolve(res.data);
      })
      .catch((error) => {
        return reject(error);
      });
  });
}

function isReference(text) {
  /*
    To check if the text passed is reference using regex
    Args: 
      * text(string) - text that needs to be checked
    Returns:
      * result(boolean) - returns true if the word is reference, false if it is not a reference
  */
  if (!text) {
    return false;
  }
  text = text.trim();
  const pattern = /^reference/i;
  let result = pattern.test(text);
  return result;
}

function isNumeric(str) {
  /*
    To check if the string is a number, used for removing the number tag in reference string
    Args: 
      * str(string) - the string we need to check
    Returns:
      * boolean - returns true if its a number, false if its not a number
  */
  if (typeof str != "string") return false; // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
}

function oneline(text) {
  /*
    to convert one or more lines to one line, removes the numbering and removes unwanted spaces
    Args: 
      * text(string) - text with n number of lines
    Returns:
      * final_str(string) - returns a string in one line
  */
  let text_arr = text.split("\n");
  let final_arr = [];
  for (let i = 0; i < text_arr.length; i++) {
    let text_arr2 = text_arr[i].split(" ");
    for (let j = 0; j < text_arr2.length; j++) {
      final_arr.push(text_arr2[j].trim());
    }
  }
  if (isNumeric(final_arr[0])) {
    final_arr.splice(0, 1);
  }
  let final_str = final_arr.join(" ");
  return final_str;
}

async function main() {
  const all_data_json = sheet_to_json("./reference-data.xlsx");
  for (let n = 0; n < all_data_json.length; n++) {
    const url = all_data_json[n].Manuscript;
    const article_id = all_data_json[n].Article_Id;
    let data;
    // console.log(url);
    // console.log(article_id);
    try {
      data = await get_data(url);
    } catch (e) {
      console.log(`Error in getting the data from the url, id= ${article_id}`);
    }
    const soup = new JSSoup(data);
    let tag = soup.nextElement;

    let i = 0;
    while (tag) {
      if (tag.text) {
        if (isReference(tag.text)) {
          // console.log("good job hommie");

          let references = tag;
          // To skip the children tags of reference
          while (isReference(tag.text) || tag.text == undefined) {
            tag = tag.nextElement;
          }

          // store the attibutes of references
          const reference_attributes = tag.attrs;

          while (tag.nextSibling) {
            if (
              tag.attrs.class == reference_attributes.class &&
              tag.attrs.style == reference_attributes.style
            ) {
              fs.appendFileSync(
                `./files/${article_id}.txt`,
                // "example_file17.txt",
                oneline(tag.text) + "\n"
              );
              // break;
            }

            // remove this to over come some test cases mentioned in the cases.txt file
            // else {
            //   break;
            // }
            tag = tag.nextSibling;
          }

          // used to test single reference
          // break;
        }
      }
      tag = tag.nextElement;
    }
    // used to test for one url
    // break;
  }
}
main();
