Court-Brown CM, Rimmer S, Prakash U, McQueen MM. The epidemiology of open long bone fractures. Injury 1998;29:529-534.
Mody RM, Zapor M, Hartzell JD, et al. Infectious complications of damage control orthopedics in war trauma. J Trauma 2009;67:758-761.
Ali AM, McMaster JM, Noyes D, Brent AJ, Cogswell LK. Experience of managing open fractures of the lower limb at a major trauma centre. Ann R Coll Surg Engl 2015;97:287-290.
Holloway KL, Yousif D, Bucki-Smith G, et al. Lower limb fracture presentations at a regional hospital. Arch Osteoporos 2017;12:75.
Louie KW. Management of open fractures of the lower limb. BMJ 2009;339:b5092.
Pollak AN, Jones AL, Castillo RC, et al. The relationship between time to surgical debridement and incidence of infection after open high-energy lower extremity trauma. J Bone Joint Surg [Am] 2010;92-A:7-15.
Parker B, Petrou S, Masters JPM, Achana F, Costa ML. Economic outcomes associated with deep surgical site infection in patients with an open fracture of the lower limb. Bone Joint J 2018;100-B:1506-1510.
MacKenzie EJ, Jones AS, Bosse MJ, et al. Health-care costs associated with amputation or reconstruction of a limb-threatening injury. J Bone Joint Surg [Am] 2007;89-A:1685-1692.
National Institute for Health and Care Excellence (NICE). Fractures (Complex): assessment and management. London: NICE, 2016. https://www.nice.org.uk/guidance/ng37 (date last accessed 9 September 2019).
Costa ML, Achten J, Bruce J, et al. Effect of negative pressure wound therapy vs standard wound management on 12-month disability among adults with severe open fracture of the lower limb: the WOLLF randomized clinical trial. JAMA 2018;319:2280-2288.
Labler L, Rancan M, Mica L, et al. Vacuum-assisted closure therapy increases local interleukin-8 and vascular endothelial growth factor levels in traumatic wounds. J Trauma 2009;66:749-757.
Tuffaha HW, Gillespie BM, Chaboyer W, Gordon LG, Scuffham PA. Cost-utility analysis of negative pressure wound therapy in high-risk cesarean section wounds. J Surg Res 2015;195:612-622.
Chatterjee A, Macarios D, Griffin L, et al. Cost-utility analysis: sartorius flap versus negative pressure therapy for infected vascular groin graft managment. Plast Reconstr Surg Glob Open 2015;3:e566.
Nherera LM, Trueman P, Karlakki SL. Cost-effectiveness analysis of single-use negative pressure wound therapy dressings (sNPWT) to reduce surgical site complications (SSC) in routine primary hip and knee replacements. Wound Repair Regen 2017;25:474-482.
Salén BA, Spangfort EV, Nygren AL, Nordemar R. The Disability Rating Index: an instrument for the assessment of disability in clinical settings. J Clin Epidemiol 1994;47:1423-1435.
National Institute for Health and Care Excellence (NICE). Guide to the methods of technology appraisal 2013. London: NICE, 2013. https://www.nice.org.uk/process/pmg9/resources/guide-to-the-methods-of-technology-appraisal-2013-pdf-2007975843781 (date last accessed 9 September 2019).
No authors listed. London: Department of Health and Social Care. Research and analysis: NHS reference costs 2014 to 2015, 2015. https://www.gov.uk/government/publications/nhs-reference-costs-2014-to-2015 (date last accessed 3 July 2019).
Curtis L, Burns A.Canterbury: Personal Social Services Research Unit (PSSRU), University of Kent. Unit costs of health and social care 2015, 2015. https://www.pssru.ac.uk/project-pages/unit-costs/unit-costs-2015/ (date last accessed 3 July 2019).
NHS Digital. Prescription costs analysis, England - 2014. London: Department of Health, 2014.https://digital.nhs.uk/data-and-information/publications/statistical/prescription-cost-analysis/prescription-cost-analysis-england-2014 (date last accessed 9 September 2019).
Department of Health. NHS Supply Chain Catalogue 2015/16. London: Department of Health, 2015.https://www.supplychain.nhs.uk/ (date last accessed 9 September 2019).
Drummond MF, Sculpher MJ, Torrance GW, O'Brien BJ, Stoddart GL.Methods for the economic evaluation of health care programmes. Third ed. Oxford: Oxford University Press, 2005.
Office for National Statistics (ONS). Annual survey of hours and earnings 2015. London: ONS, 2015.https://beta.ukdataservice.ac.uk/datacatalogue/doi/?id=6706#!#7 (date last accessed 9 September 2019).
Brooks R. EuroQol: the current state of play. Health Policy 1996;37:53-72.
Dolan P. Modeling valuations for EuroQol health states. Med Care 1997;35:1095-1108.
Manca A, Hawkins N, Sculpher MJ. Estimating mean QALYs in trial-based cost-effectiveness analysis: the importance of controlling for baseline utility. Health Econ 2005;14:487-496.
Ware J, Jr., Kosinski M, Keller SD. A 12-Item Short-Form Health Survey: Construction of scales and preliminary tests of reliability and validity. Medical Care 1996; 34: 220-233.
Brazier JE, Roberts J. The estimation of a preference-based measure of health from the SF-12. Med Care 2004;42:851-859.
White IR, Royston P, Wood AM. Multiple imputation using chained equations: issues and guidance for practice. Stat Med 2011;30:377-399.
Gustilo RB, Anderson JT. JSBS classics. Prevention of infection in the treatment of one thousand and twenty-five open fractures of long bones. Retrospective and prospective analyses. J Bone Joint Surg [Am] 2002;84-A:682.
Claxton K, Martin S, Soares M, et al. Methods for the estimation of the National Institute for Health and Care Excellence cost-effectiveness threshold. Health Technol Assess 2015;19:1-503, v-vi.
Barber JA, Thompson SG. Analysis of cost data in randomized trials: an application of the non-parametric bootstrap. Stat Med 2000;19:3219-3236.
Black WC. The CE plane: a graphic representation of cost-effectiveness. Med Decis Making 1990;10:212-214.
Stinnett AA, Mullahy J. Net health benefits: a new framework for the analysis of uncertainty in cost-effectiveness analysis. Med Decis Making 1998;18(2 Suppl):S68-S80.
Fenwick E, Claxton K, Sculpher M. Representing uncertainty: the role of cost-effectiveness acceptability curves. Health Econ 2001;10:779-787.
Horan TC, Andrus M, Dudeck MA. CDC/NHSN surveillance definition of health care-associated infection and criteria for specific types of infections in the acute care setting. Am J Infect Control 2008;36:309-332.
Tissingh EK, Memarzadeh A, Queally J, Hull P. Open lower limb fractures in major trauma centers - a loss leader? Injury 2017;48:353-356.
An TJ, Thakore RV, Greenberg SE, et al. Locking versus nonlocking implants in isolated lower extremity fractures: analysis of cost and complications. J Surg Orthop Adv 2016;25:49-53.
Husereau D, Drummond M, Petrou S, et al. Consolidated Health Economic Evaluation Reporting Standards (CHEERS)--explanation and elaboration: a report of the ISPOR Health Economic Evaluation Publication Guidelines Good Reporting Practices Task Force. Value Health 2013;16:231-250.
Petrou S, Murray L, Cooper P, Davidson LL. The accuracy of self-reported healthcare resource utilization in health economic studies. Int J Technol Assess Health Care 2002;18:705-710.
Faria R, Gomes M, Epstein D, White IR. A guide to handling missing data in cost-effectiveness analysis conducted within randomised controlled trials. Pharmacoeconomics 2014;32:1157-1170.
